Squirrel - A Next-Generation Data Warehouse for Debian
======================================================

This repository is the home of the data warehouse that powers the
Lintian web site.

Like [UDD](https://wiki.debian.org/UltimateDebianDatabase) we collect
data from across the Debian ecosystem. Our approach differs in two
aspects:

* We collect both static and event-driven data.
* All data sources are cross-linked and therefore easier to query.

Questions or Problems?
======================

Please contact the
[Lintian Maintainers](https://qa.debian.org/developer.php?email=lintian-maint%40debian.org)
for additional needs.
